﻿Shader "JM/URP/Lit_VertexAnimation"{
    Properties
    { 
		[Header(Lighting)][HDR]_LightColor("Normal Lighting Color", Color) = (1,1,1,1)
		_Metallic("Metallic",Range(0,1)) = 0
		_Smoothness("Smoothness",Range(0,1)) = 0.5
		_LightMult("Incoming Light Multiplicator", float) = 1
		[Header(VertexData)][NoScaleOffset]_PostionTexture("Position Texture", 2D) = "white" {}
		[NoScaleOffset]_NormalTexture("Normal Texture", 2D) = "white" {}
		_Bounds("Bounds", float) = 10
		_TextureWidth("Texture Width", float) = 128
		_NumFrames("Texture Height", float) = 128
		_Frame("Frame", float) = 1
		_Speed("Speed (FPS)", float) = 24
		_Offset("Position Offset", vector) = (0,0,0,0)
		_ScaleOffset("Scale Offset",vector) = (1,1,1,0)
		[Header(Texturing)]_BaseMap("Base Map", 2D) = "white" {}
		_Tint("Tint", Color) = (1,1,1,1)
		[Header(Sway)]_SwaySpeed("Sway Speed",float) = 50
		_SwayStrength("Sway Strength",float) = 0.1
		[Header(Fresnel)][HDR]_FresnelCol("Fresnel Color", Color) = (1,1,1,1)
		_FresnelPower("Fresnel Power",Range(0,1)) = 1
		_FresnelVertColPower("Fresnel Vertex Color Power", Range(0,10)) = 3
		[Header(Dissolve)]_DissolveAmount("Dissolve Amount",Range(0,1)) = 0
		_DissolveNoise("Dissolve Noise",float) = 200
		[HDR]_DissolveEdgeColor("Dissolve Edge Color", Color) = (1,1,1,1)
		_DissolveEdgeThickness("Dissolve Edge Thickness",Range(0,2)) = 0.3
		[Header(Animationsplit)]
		_AnimationStart("Animation Start Frame",Range(0,1)) = 0
		_AnimationEnd("Animation End Frame",Range(0,1)) = 1
	}

    SubShader
    {
        Tags { "RenderType" = "Transparent" "RenderPipeline" = "UniversalRenderPipeline" "Queue" = "Transparent+0"}
		
        Pass
        {
			Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
			Cull Off
			ZTest LEqual
			ZWrite On

            HLSLPROGRAM
			#pragma prefer_hlslcc gles
            #pragma vertex vert
            #pragma fragment frag

			#pragma multi_compile_instancing
			#pragma multi_compile_fog

			#pragma shader_feature USE_NORMAL
			#pragma shader_feature SWAY
			#pragma shader_feature FRESNEL
			#pragma shader_feature DISSOLVE
			#pragma shader_feature ANIMATION_FRAC

			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile _ _ADDITIONAL_LIGHTS

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			#include "Includes/CustomMath.hlsl"
			#include "Includes/CustomNoise.hlsl"
            

            CBUFFER_START(UnityPerMaterial)
				TEXTURE2D(_PostionTexture);
				SAMPLER(sampler_PostionTexture);
				float4 _PostionTexture_ST;

				TEXTURE2D(_NormalTexture);
				SAMPLER(sampler_NormalTexture);
				float4 _NormalTexture_ST;

				TEXTURE2D(_BaseMap);
				SAMPLER(sampler_BaseMap);
				float4 _BaseMap_ST;

				float _Bounds,
					_TextureWidth,
					_NumFrames,
					_Frame,
					_Speed,
					_LightMult,
					_Metallic,
					_Smoothness,
					_SwaySpeed,
					_SwayStrength,
					_FresnelPower,
					_FresnelVertColPower,
					_DissolveAmount,
					_DissolveNoise,
					_DissolveEdgeThickness,
					_AnimationStart,
					_AnimationEnd;

				half4 _Tint,
					_LightColor,
					_Offset,
					_ScaleOffset,
					_FresnelCol,
					_DissolveEdgeColor;
            CBUFFER_END			

            struct Attributes
            {
                float4 positionOS   : POSITION;
				float2 uv  : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
				float2 uv2 : TEXCOORD2;
				float3 normal : NORMAL;
				float3 tangent : TANGENT;
				float4 color : COLOR;
				float3 viewDir : TEXCOORD3;
            };

            struct Varyings
            {
                float4 positionHCS  : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
				float2 uv2 : TEXCOORD2;
				float3 viewDir : TEXCOORD3;
				float fogFactor : TEXCOORD4;
				float3 normal : NORMAL;
				float3 tangent : TANGENT;
				float4 color : COLOR;
				float4 shadowCoord : TEXCOORD5;
            }; 			
            Varyings vert(Attributes IN)
            {
                Varyings OUT = (Varyings)0;
				VertexPositionInputs vertInput = GetVertexPositionInputs(IN.positionOS.xyz);
				float epsilon = 0.0001;
				float pixel = 1.0/ (_TextureWidth + 1);
				float half_pixel = pixel * 0.5;
				float frame_pixel_size = 1.0/_NumFrames;

#if ANIMATION_FRAC
				_Speed /= (_AnimationEnd + epsilon);
#endif
				float2 tex_coord = float2(frac(IN.uv1.x + half_pixel),-frac((_Time.y* _Speed + 0.5 + _Frame) * frame_pixel_size));
#if ANIMATION_FRAC
				tex_coord.y = remap(tex_coord.y, 0, 1, _AnimationStart, _AnimationEnd);
#endif

				
				float3 pos = SAMPLE_TEXTURE2D_LOD(_PostionTexture,sampler_PostionTexture,tex_coord,0).rgb;
				pos = remap3(pos,0,1,_Bounds * -1,_Bounds);
				pos.x *= -1;
				pos.xyz *= _ScaleOffset.xyz;
				pos.xyz += _Offset.xyz;

				float3 nrm = SAMPLE_TEXTURE2D_LOD(_NormalTexture,sampler_NormalTexture,tex_coord,0).rgb;
				float nrm_x = (nrm.x * 2) - 1;
				float nrm_y = (nrm.y * 2) - 1;
				float nrm_z = (nrm.z * 2) - 1;
				float3 objectNormal = float3(nrm_x,nrm_y,nrm_z);
				OUT.normal = TransformObjectToWorldNormal(objectNormal);
#if SWAY
				float x = sin(vertInput.positionWS.x + (_Time.x * _SwaySpeed)) * IN.color.r;
				float z = sin(vertInput.positionWS.z + (_Time.x * _SwaySpeed)) * IN.color.r;
				pos.x += step(0, IN.color.r) * x * _SwayStrength;
				pos.z += step(0, IN.color.r) * z * _SwayStrength;
#endif
#if FRESNEL
				OUT.viewDir = normalize(_WorldSpaceCameraPos - vertInput.positionWS.xyz);
#endif
#ifdef _MAIN_LIGHT_SHADOWS
				OUT.shadowCoord = GetShadowCoord(vertInput);
#endif

				OUT.color = IN.color;
				OUT.uv = TRANSFORM_TEX(IN.uv, _BaseMap);
                OUT.positionHCS = TransformObjectToHClip(pos);
				OUT.fogFactor = ComputeFogFactor(OUT.positionHCS.z);
                return OUT;
            }

            half4 frag(Varyings IN) : SV_Target
            {
				half4 surface = SAMPLE_TEXTURE2D(_BaseMap,sampler_BaseMap,IN.uv) * _Tint;
#if USE_NORMAL
				surface.rgb *= pow(clamp(IN.normal.x, 0, 1),2) + 0.5;
				surface.rgb = clamp(surface.rgb * _LightColor.rgb, 0, 1);
#endif
#if FRESNEL
				half fresnel = getFresnel(IN.viewDir, IN.normal,_FresnelPower) * pow(IN.color.g, abs(_FresnelVertColPower));
				surface.rgb += fresnel * _FresnelCol.rgb;
#endif
#if DISSOLVE
				half noise = step(PerlinNoise(IN.uv * _DissolveNoise), remap(_DissolveAmount, 0, 1, -0.1, 1.8));
				half noise2 = step(PerlinNoise(IN.uv * _DissolveNoise), remap(_DissolveAmount, 0, 1 + _DissolveEdgeThickness, -0.1, 1.8));
				half mask = (noise - noise2);
				surface.rgb = lerp(surface.rgb, _DissolveEdgeColor.rgb, mask);
				surface.a = lerp(surface.a, 0, noise2);
#endif
#ifdef LIGHTMAP_ON
				half3 bakedGI = SampleLightmap(input.uv2, normalize(IN.normal));
#else
				half3 bakedGI = SampleSH(normalize(IN.normal));
#endif
				BRDFData brdfData;
				InitializeBRDFData(surface.rgb, _Metallic, 0, _Smoothness, surface.a, brdfData);

#ifdef _MAIN_LIGHT_SHADOWS
				Light mainLight = GetMainLight(IN.shadowCoord);
#else
				Light mainLight = GetMainLight();
#endif
				half3 color = GlobalIllumination(brdfData, bakedGI, 1, normalize(IN.normal), IN.viewDir);
				color += LightingPhysicallyBased(brdfData, mainLight, normalize(IN.normal), IN.viewDir);

#ifdef _ADDITIONAL_LIGHTS
				int additionalLightsCount = GetAdditionalLightsCount();
				for (int i = 0; i < additionalLightsCount; ++i)
				{
					Light light = GetAdditionalLight(i, IN.positionHCS.xyz);
					color += LightingPhysicallyBased(brdfData, light, normalize(IN.normal), IN.viewDir);
				}
#endif
				surface.rgb += color * _LightMult;
				surface.rgb = MixFog(surface.rgb, IN.fogFactor);
				return surface;
            }
            ENDHLSL
        }		
		//UsePass "Universal Render Pipeline/Lit/ShadowCaster" //this pass is static and does not take into account any vertex animations Also it breaks the srp batching.
		//UsePass "Universal Render Pipeline/Lit/DepthOnly" //this pass is static and does not take into account any vertex animations. Also it breaks the srp batching.
    }
	CustomEditor "Unlit_VertexAnimation_Editor"
}