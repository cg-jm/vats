/// creating noise functions (may be to expensive)
float2 Noise_Direction(float2 p){
    p = p % 289;
    float x = (34 * p.x + 1) * p.x % 289 + p.y;
    x = (34 * x + 1) * x % 289;
    x = frac(x / 41) * 2 - 1;
    return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
}

float PerlinNoise(float2 uv){
    float2 ip = floor(uv);
    float2 fp = frac(uv);
    float dot_00 = dot(Noise_Direction(ip),fp);
    float dot_01 = dot(Noise_Direction(ip + float2(0,1)), fp - float2(0,1));
    float dot_10 = dot(Noise_Direction(ip + float2(1,0)), fp - float2(1,0));
    float dot_11 = dot(Noise_Direction(ip + float2(1,1)), fp - float2(1,1));
    fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
    return lerp(lerp(dot_00, dot_01, fp.y), lerp(dot_10, dot_11, fp.y), fp.x) + 0.5;
}
/// end creating noise functions