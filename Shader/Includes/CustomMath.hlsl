half4 remap4(half4 value,half old_min, half old_max,half new_min, half new_max){
    half x = new_min + (value.x - old_min) * (new_max - new_min) / (old_max - old_min);
    half y = new_min + (value.y - old_min) * (new_max - new_min) / (old_max - old_min);
    half z = new_min + (value.z - old_min) * (new_max - new_min) / (old_max - old_min);
    half w = new_min + (value.w - old_min) * (new_max - new_min) / (old_max - old_min);
    return half4(x,y,z,w);
}

half3 remap3(half3 value,half old_min, half old_max,half new_min, half new_max){
    half x = new_min + (value.x - old_min) * (new_max - new_min) / (old_max - old_min);
    half y = new_min + (value.y - old_min) * (new_max - new_min) / (old_max - old_min);
    half z = new_min + (value.z - old_min) * (new_max - new_min) / (old_max - old_min);
    return half3(x,y,z);
}

half2 remap2(half2 value,half old_min, half old_max,half new_min, half new_max){
    half x = new_min + (value.x - old_min) * (new_max - new_min) / (old_max - old_min);
    half y = new_min + (value.y - old_min) * (new_max - new_min) / (old_max - old_min);
    return half2(x,y);
}

half remap(half value,half old_min, half old_max,half new_min, half new_max){
    return new_min + (value - old_min) * (new_max - new_min) / (old_max - old_min);
}

half getFresnel(half3 viewDir,half3 normal, half amount){
	return 1 - clamp(pow(abs(dot(viewDir,normal)),amount),0,1);
}