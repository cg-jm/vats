﻿using System;
using UnityEngine;
using UnityEditor;

public class Unlit_VertexAnimation_Editor : ShaderGUI {

    bool showVertexData = false;
    bool showBaseMap = false;

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties) {
        Material target = materialEditor.target as Material;
        bool use_normal = Array.IndexOf(target.shaderKeywords, "USE_NORMAL") != -1;
        bool sway = Array.IndexOf(target.shaderKeywords, "SWAY") != -1;
        bool fresnel = Array.IndexOf(target.shaderKeywords, "FRESNEL") != -1;
        bool dissolve = Array.IndexOf(target.shaderKeywords, "DISSOLVE") != -1;
        bool anim_split = Array.IndexOf(target.shaderKeywords, "ANIMATION_FRAC") != -1;

        GUILayout.Space(10);
        EditorGUILayout.LabelField("Shader Features", EditorStyles.boldLabel);
        use_normal = EditorGUILayout.Toggle("Use Normal Lighting", use_normal);
        if (use_normal) {
            target.EnableKeyword("USE_NORMAL");

        }
        else {
            target.DisableKeyword("USE_NORMAL");
        }

        sway = EditorGUILayout.Toggle("Use Wind Sway", sway);
        if (sway) {
            target.EnableKeyword("SWAY");
        }
        else {
            target.DisableKeyword("SWAY");
        }        

        fresnel = EditorGUILayout.Toggle("Use Fresnel", fresnel);
        if (fresnel) {
            target.EnableKeyword("FRESNEL");
        }
        else {
            target.DisableKeyword("FRESNEL");
        }

        dissolve = EditorGUILayout.Toggle("Use Dissolve", dissolve);
        if (dissolve) {
            target.EnableKeyword("DISSOLVE");
        }
        else {
            target.DisableKeyword("DISSOLVE");
        }

        anim_split = EditorGUILayout.Toggle("Use Animation Splitting", anim_split);
        if (anim_split) {
            target.EnableKeyword("ANIMATION_FRAC");
        }
        else {
            target.DisableKeyword("ANIMATION_FRAC");
        }
        GUILayout.Space(10);
        showVertexData = EditorGUILayout.Foldout(showVertexData,"Vertex Data");
        if (showVertexData) {
            MaterialProperty _PostionTexture = FindProperty("_PostionTexture", properties);
            materialEditor.ShaderProperty(_PostionTexture, _PostionTexture.displayName);
            MaterialProperty _NormalTexture = FindProperty("_NormalTexture", properties);
            materialEditor.ShaderProperty(_NormalTexture, _NormalTexture.displayName);
            MaterialProperty _Bounds = FindProperty("_Bounds", properties);
            materialEditor.ShaderProperty(_Bounds, _Bounds.displayName);
            MaterialProperty _TextureWidth = FindProperty("_TextureWidth", properties);
            materialEditor.ShaderProperty(_TextureWidth, _TextureWidth.displayName);
            MaterialProperty _NumFrames = FindProperty("_NumFrames", properties);
            materialEditor.ShaderProperty(_NumFrames, _NumFrames.displayName);
            MaterialProperty _Frame = FindProperty("_Frame", properties);
            materialEditor.ShaderProperty(_Frame, _Frame.displayName);
            MaterialProperty _Speed = FindProperty("_Speed", properties);
            materialEditor.ShaderProperty(_Speed, _Speed.displayName);
            MaterialProperty _Offset = FindProperty("_Offset", properties);
            _Offset.vectorValue = EditorGUILayout.Vector3Field(_Offset.displayName, _Offset.vectorValue);
            MaterialProperty _ScaleOffset = FindProperty("_ScaleOffset", properties);
            _ScaleOffset.vectorValue = EditorGUILayout.Vector3Field(_ScaleOffset.displayName, _ScaleOffset.vectorValue);
        }
        GUILayout.Space(10);
        showBaseMap = EditorGUILayout.Foldout(showBaseMap, "Texturing");
        if (showBaseMap) {
            MaterialProperty _BaseMap = FindProperty("_BaseMap", properties);
            materialEditor.ShaderProperty(_BaseMap, _BaseMap.displayName);
            MaterialProperty _Tint = FindProperty("_Tint", properties);
            materialEditor.ShaderProperty(_Tint, _Tint.displayName);
        }

        //paint properties
        if (use_normal) {
            MaterialProperty _LightColor = FindProperty("_LightColor", properties);
            materialEditor.ShaderProperty(_LightColor, _LightColor.displayName);
        }
        if (sway) {
            MaterialProperty _SwaySpeed = FindProperty("_SwaySpeed", properties);
            materialEditor.ShaderProperty(_SwaySpeed, _SwaySpeed.displayName);
            MaterialProperty _SwayStrength = FindProperty("_SwayStrength", properties);
            materialEditor.ShaderProperty(_SwayStrength, _SwayStrength.displayName);
        }
        if (fresnel) {
            MaterialProperty _FresnelCol = FindProperty("_FresnelCol", properties);
            materialEditor.ShaderProperty(_FresnelCol, _FresnelCol.displayName);
            MaterialProperty _FresnelPower = FindProperty("_FresnelPower", properties);
            materialEditor.ShaderProperty(_FresnelPower, _FresnelPower.displayName);
            MaterialProperty _FresnelVertColPower = FindProperty("_FresnelVertColPower", properties);
            materialEditor.ShaderProperty(_FresnelVertColPower, _FresnelVertColPower.displayName);
        }        
        if (dissolve) {
            MaterialProperty _DissolveAmount = FindProperty("_DissolveAmount", properties);
            materialEditor.ShaderProperty(_DissolveAmount, _DissolveAmount.displayName);
            MaterialProperty _DissolveNoise = FindProperty("_DissolveNoise", properties);
            materialEditor.ShaderProperty(_DissolveNoise, _DissolveNoise.displayName);
            MaterialProperty _DissolveEdgeColor = FindProperty("_DissolveEdgeColor", properties);
            materialEditor.ShaderProperty(_DissolveEdgeColor, _DissolveEdgeColor.displayName);
            MaterialProperty _DissolveEdgeThickness = FindProperty("_DissolveEdgeThickness", properties);
            materialEditor.ShaderProperty(_DissolveEdgeThickness, _DissolveEdgeThickness.displayName);
        }
        if (anim_split) {
            MaterialProperty _AnimationStart = FindProperty("_AnimationStart", properties);
            materialEditor.ShaderProperty(_AnimationStart, _AnimationStart.displayName);
            MaterialProperty _AnimationEnd = FindProperty("_AnimationEnd", properties);
            materialEditor.ShaderProperty(_AnimationEnd, _AnimationEnd.displayName);
        }

        //base.OnGUI(materialEditor, properties);

        GUILayout.Space(10);
    }
}
