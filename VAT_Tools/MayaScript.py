import maya.cmds as cmds
import maya.mel as mel
import os
import subprocess

#vars
minplayback = cmds.playbackOptions( q=True,min=True )
maxplayback = cmds.playbackOptions( q=True,max=True )

if cmds.window("VertIdWindow", exists = True):
    cmds.deleteUI("VertIdWindow")    

window = cmds.window("VertIdWindow", w = 605, h = 305, mnb = False, mxb = False)
cmds.rowColumnLayout(numberOfColumns = 1)
cmds.separator(height=40)
object_save = cmds.textFieldGrp(label="Save Data",text="C:/Users/Jeremias Meister/Downloads", h=20)
cmds.button(l="Select Dir", c="select_dir()")
animFrom = cmds.intSliderGrp(label="Sample From",f=True,min=minplayback,max=maxplayback,v=minplayback)
animTo = cmds.intSliderGrp(label="Sample To",f=True,min=minplayback,max=maxplayback,v=maxplayback)
def_frame = cmds.intSliderGrp(label="Static Frame",f=True,min=minplayback,max=maxplayback,v=minplayback)
remapVals = cmds.checkBoxGrp(label="Dont remap",v1 = False)
cmds.separator(height=40)
cmds.button(l="Sample Data", c="data_export()")
cmds.showWindow("VertIdWindow")

def copy2clip(txt):
    cmd='echo '+txt.strip()+'|clip'
    return subprocess.check_call(cmd, shell=True)

def select_dir():
    path = cmds.fileDialog2(cap="Select Directory to Save VAT Data...",fm=3,okc="Select Dir",cc="Cancel")
    if not path:
        return
    path = path[0]
    if os.path.isdir(path):
        cmds.textFieldGrp(object_save,edit=True,text=path)
    else:
        cmds.textFieldGrp(object_save,edit=True,text="Invalid Path")

def data_export():
    obj = cmds.ls(sl=True,fl=True)[0]
    default_frame = cmds.intSliderGrp(def_frame,q=True,v=True)
    if not cmds.objExists(obj):
        print("WARNING!: No object to process selected. Aborting...")
        return None

    animfrom = cmds.intSliderGrp(animFrom,q=True,v=True)
    animto = cmds.intSliderGrp(animTo,q=True,v=True)
    output_dir = cmds.textFieldGrp(object_save,query=True,text=True)

    frame_range = [animfrom,animto]
    bound_scale = get_animation_bounds(obj,frame_range)

    sample_data(obj,False,bound_scale,frame_range,output_dir)
    sample_data(obj,True,bound_scale,frame_range,output_dir)

    mesh = new_obj_from_frame(obj,default_frame)
    create_morph_uv_set(mesh,False)
    cmds.select(mesh)
    mesh = cmds.rename(mesh, "%s_animationMesh" %''.join(obj))

    filepath = output_dir + "/%s_bounds.txt" %''.join(obj)
    with open(filepath,"w") as f:
        f.write(str(bound_scale))
    export_mesh(mesh)


#my way , the old way to do the magic
def sample_data(obj, is_normal, bound_scale,frame_range,output_dir):    
    straight_data = cmds.checkBoxGrp(remapVals,query=True,v1=True)
    vtx = cmds.ls(obj+'.vtx[*]',fl=True)    
    writelist = ""    
    for j in range(frame_range[0],frame_range[1]+1):
        cmds.currentTime(j)
        for i in range(len(vtx)):
            attr = []
            if straight_data:                
                if is_normal:
                    attr = cmds.polyNormalPerVertex(vtx[i], query=True,xyz=True)
                else:
                    attr = cmds.xform(vtx[i],q=True,t=True,os=True)
            else:
                if is_normal:
                    attr = unsign_vector(cmds.polyNormalPerVertex(vtx[i], query=True,xyz=True))
                else:
                    attr = unsign_vector(cmds.xform(vtx[i],q=True,t=True,os=True),True,bound_scale)
            
            vertexInfo = [i,j,attr]
            writelist += str(vertexInfo) + "\n"

    filepath = ""
    if is_normal: filepath = output_dir + "/%s_normal.txt" %''.join(obj)
    else: filepath = output_dir + "/%s_position.txt" %''.join(obj)
    with open(filepath,"w") as f:
        f.write(writelist)


def new_obj_from_frame(obj,frame):
    cmds.currentTime(frame)
    return_obj = cmds.duplicate(obj,ic=False,un=False)
    return return_obj

def unsign_vector(vector,use_remap=False,bound_scale = 1):
    if use_remap:
        vector[0] = remap(vector[0],-bound_scale,bound_scale,0,1)
        vector[1] = remap(vector[1],-bound_scale,bound_scale,0,1)
        vector[2] = remap(vector[2],-bound_scale,bound_scale,0,1)
    else:
        vector = [vector[0]+1.0,vector[1]+1.0,vector[2]+1.0]
        vector = [vector[0]/2,vector[1]/2,vector[2]/2]
    return vector

def remap(value,min_1,max_1,min_2,max_2):
    return min_2 + (value - min_1) * (max_2 - min_2) / (max_1 - min_1)

#this currently does not check if a second uv set already exists. it creates a new uv set none or less
def create_morph_uv_set(obj,rebuild_uv0):
    cmds.select(obj)
    if rebuild_uv0 : cmds.polyPlanarProjection(obj)
    cmds.polyUVSet(create=True, uvSet='map2')
    cmds.polyUVSet( currentUVSet=True, uvSet='map2')
    cmds.polyPlanarProjection(obj)

    sel = cmds.ls(sl=True)[0]
    vtx = cmds.ls(sel+'.vtx[*]',fl=True)

    offset = (1.00000000/len(vtx))/2
    for v in range(len(vtx)):
        cmds.select(vtx[v])
        cmds.ConvertSelectionToUVs()
        cmds.polyEditUV(relative=False, uValue=(float(v)/len(vtx))+offset,vValue=0.5)

def export_mesh(obj):
    cmds.select(obj)
    path = cmds.textFieldGrp(object_save,q=True,text=True)
    path = path + "/" + obj + ".fbx"
    path = path.replace("_animationMesh","")
    copy2clip(path)
    print("Path is copied to Clipboard")
    mel.eval('ExportSelection;')

def get_animation_bounds(obj,frame_range):
    bound_scale = 0
    cmds.select(obj)
    sel = cmds.ls(sl=True,fl=True)[0]
    vtx = cmds.ls(sel+'.vtx[*]',fl=True)
    for i in range(frame_range[0],frame_range[1]):
        cmds.currentTime(i)
        for v in range(len(vtx)):
            pos = cmds.xform(vtx[v],q=True,t=True)
            if pos[0] > bound_scale: bound_scale = pos[0]
            if pos[1] > bound_scale: bound_scale = pos[1]
            if pos[2] > bound_scale: bound_scale = pos[2]
    return bound_scale
