# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'TextureCreatorUI.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog
import ast
from PIL import Image
import numpy as np
import imageio


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(394, 335)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(49, 49, 49))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(49, 49, 49))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(49, 49, 49))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(49, 49, 49))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWindow.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        MainWindow.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("icon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 20, 91, 21))
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label.setObjectName("label")
        self.input_dir = QtWidgets.QLineEdit(self.centralwidget)
        self.input_dir.setGeometry(QtCore.QRect(122, 20, 181, 21))
        self.input_dir.setObjectName("input_dir")
        self.pos_data = QtWidgets.QLineEdit(self.centralwidget)
        self.pos_data.setGeometry(QtCore.QRect(120, 90, 121, 20))
        self.pos_data.setObjectName("pos_data")
        self.nrm_data = QtWidgets.QLineEdit(self.centralwidget)
        self.nrm_data.setGeometry(QtCore.QRect(120, 120, 121, 20))
        self.nrm_data.setObjectName("nrm_data")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(250, 90, 71, 20))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(250, 120, 71, 20))
        self.label_3.setObjectName("label_3")
        self.object_name = QtWidgets.QLineEdit(self.centralwidget)
        self.object_name.setGeometry(QtCore.QRect(120, 60, 121, 20))
        self.object_name.setObjectName("object_name")
        self.paste_object_name_btn = QtWidgets.QPushButton(self.centralwidget)
        self.paste_object_name_btn.setGeometry(QtCore.QRect(250, 60, 131, 23))
        self.paste_object_name_btn.setObjectName("paste_object_name_btn")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(20, 60, 91, 21))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(20, 90, 91, 21))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(20, 120, 91, 21))
        self.label_6.setObjectName("label_6")
        self.is_exr = QtWidgets.QCheckBox(self.centralwidget)
        self.is_exr.setGeometry(QtCore.QRect(120, 150, 121, 21))
        self.is_exr.setObjectName("is_exr")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(20, 150, 101, 21))
        self.label_7.setObjectName("label_7")
        self.select_input_dir = QtWidgets.QPushButton(self.centralwidget)
        self.select_input_dir.setGeometry(QtCore.QRect(310, 20, 25, 21))
        self.select_input_dir.setObjectName("select_input_dir")
        self.process_btn = QtWidgets.QPushButton(self.centralwidget)
        self.process_btn.setGeometry(QtCore.QRect(20, 182, 361, 41))
        self.process_btn.setDefault(False)
        self.process_btn.setFlat(False)
        self.process_btn.setObjectName("process_btn")
        self.debug_message = QtWidgets.QLabel(self.centralwidget)
        self.debug_message.setGeometry(QtCore.QRect(20, 240, 361, 21))
        self.debug_message.setObjectName("debug_message")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 394, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.connect_buttons()
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Process VAT Data from Maya"))
        self.label.setText(_translate("MainWindow", "Input Directory"))
        self.label_2.setText(_translate("MainWindow", "_position.txt"))
        self.label_3.setText(_translate("MainWindow", "_normal.txt"))
        self.paste_object_name_btn.setText(_translate("MainWindow", "Set Position/Normal"))
        self.label_4.setText(_translate("MainWindow", "Object Name"))
        self.label_5.setText(_translate("MainWindow", "Position Data"))
        self.label_6.setText(_translate("MainWindow", "Normal Data"))
        self.is_exr.setText(_translate("MainWindow", ".Exr"))
        self.label_7.setText(_translate("MainWindow", "Export Exr (Beta)"))
        self.select_input_dir.setText(_translate("MainWindow", "..."))
        self.process_btn.setText(_translate("MainWindow", "Process"))
        self.debug_message.setText(_translate("MainWindow", "Fill all Inputs with data and press process..."))

    def connect_buttons(self):
        self.select_input_dir.clicked.connect(self.choose_dir)
        self.paste_object_name_btn.clicked.connect(self.set_inputs)
        self.process_btn.clicked.connect(self.process_images)

    def choose_dir(self):
        folder = str(QFileDialog.getExistingDirectory(None, "Select Directory"))
        self.input_dir.setText(folder)

    def set_inputs(self):
        n = self.object_name.text()
        self.pos_data.setText(n)
        self.nrm_data.setText(n)

    def process_images(self):
        self.setDebug("Processing...")
        try:
            processor = RetranslateVATToTex(self.object_name.text(),
                                            self.input_dir.text(),
                                            self.pos_data.text(),
                                            self.nrm_data.text(),
                                            self.is_exr.isChecked())
            self.setDebug("Done Processing")
        except:
            self.setDebug("An Error occured. Check the inputs and try again...")

    def setDebug(self, text):
        self.debug_message.setText(text)
        self.debug_message.repaint()



class RetranslateVATToTex():
    def __init__(self,filename,file_dir,file_pos,file_normal,is_exr=False):
        self.filename = filename
        self.file_dir = file_dir
        self.file_pos = file_pos + "_position.txt"
        self.file_normal = file_normal + "_normal.txt"
        self.process_info(self.file_dir,self.file_pos,is_exr)
        self.process_info(self.file_dir,self.file_normal,is_exr)

    def process_info(self,f_dir,f_name, is_exr):
        filepath = f_dir + "/" + f_name + ".txt"
        filepath = filepath.replace("//","/")
        filepath = filepath.replace(".txt.txt",".txt")
        self.create_image(filepath,is_exr)


    def remap(self,value,min_1,max_1,min_2,max_2):
        return min_2 + (value - min_1) * (max_2 - min_2) / (max_1 - min_1)

    def create_image(self,filepath, is_exr):
        rawData = []
        with open(filepath,"r") as f:
            rawData = f.readlines()

        attributes = []
        for i in rawData:
            val = i.replace('\n','')
            val = ast.literal_eval(val)
            attributes.append(val)


        if not is_exr:
            size = (attributes[-1][0]+1, attributes[-1][1])
            img = Image.new('RGB',size,color='black')
            img.resize(size)
            print(size)
            pix = img.load()
            filepath = filepath.replace(".txt",".png")

            for i in range(0,len(attributes)): #verts len -1 since we start from 0
                x = attributes[i][0]
                y = attributes[i][1]
                rgb = (int(self.remap(attributes[i][2][0],0,1,0,255)),int(self.remap(attributes[i][2][1],0,1,0,255)),int(self.remap(attributes[i][2][2],0,1,0,255)))
                if x >= 0 and y >= 1:
                    if x <= size[0] and y <= size[1]:
                        pix[x,y-1] = rgb

            img.save(filepath)

            print("Done")
        #currently exr does not work -> needs more investigation
        else:
            size = (attributes[-1][1]+1, attributes[-1][0]+1,3) #third dimension is for channel amount 012 == rgb
            filepath = filepath.replace(".txt",".exr")
            print(size)
            rgbArray = np.random.uniform(0.0000,1.0000,size=size)
            rgbArray = rgbArray.astype("float32")
            for i in range(0,len(attributes)):
                x = attributes[i][1]
                y = attributes[i][0]
                rgbArray[x,y,0] = self.remap(attributes[i][2][0],0,1,0.0000,1.0000) #r
                rgbArray[x,y,1] = self.remap(attributes[i][2][1],0,1,0.0000,1.0000) #g
                rgbArray[x,y,2] = self.remap(attributes[i][2][2],0,1,0.0000,1.0000) #b
            imageio.imwrite(filepath, rgbArray)
            print("Done")

        
        

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
